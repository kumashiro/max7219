/*
 * Copyright (C) 2014 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License Version
 * 3.0 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <avr/io.h>
#include <util/delay.h>

#include "max7219_config.h"
#include "max7219.h"

#ifndef MAX7219_DIGITS
#error Configuration error: Macro MAX7219_DIGITS not defined
#endif

#ifndef MAX7219_PORT_DATA
#error Configuration error: Macro MAX7219_PORT_DATA not defined
#endif

#ifndef MAX7219_DREG_DATA
#error Configuration error: Macro MAX7219_DREG_DATA not defined
#endif

#ifndef MAX7219_PIN_DATA
#error Configuration error: Macro MAX7219_PIN_DATA not defined
#endif

#ifndef MAX7219_PORT_CLK
#error Configuration error: Macro MAX7219_PORT_CLK not defined
#endif

#ifndef MAX7219_DREG_CLK
#error Configuration error: Macro MAX7219_DREG_CLK not defined
#endif

#ifndef MAX7219_PIN_CLK
#error Configuration error: Macro MAX7219_PIN_CLK not defined
#endif

#ifndef MAX7219_PORT_LOAD
#error Configuration error: Macro MAX7219_PORT_LOAD not defined
#endif

#ifndef MAX7219_DREG_LOAD
#error Configuration error: Macro MAX7219_DREG_LOAD not defined
#endif

#ifndef MAX7219_PIN_LOAD
#error Configuration error: Macro MAX7219_PIN_LOAD not defined
#endif


void  max7219_init(void)
{
    MAX7219_DREG_DATA |= (1 << MAX7219_PIN_DATA);
    MAX7219_DREG_CLK |= (1 << MAX7219_PIN_CLK);
    MAX7219_DREG_LOAD |= (1 << MAX7219_PIN_LOAD);

#if defined MAX7219_BOOT_TIME && MAX7219_BOOT_TIME > 0
    /* Wait for MAX7219 chip to settle down */
    _delay_ms(MAX7219_BOOT_TIME);
#endif

    /* We support only 7-segment LED displays in BCD mode */
    max7219_push(MAX7219_ADDR_DECMODE, 15);

    /* Set the number of digits */
    max7219_push(MAX7219_ADDR_SCANLIM, MAX7219_DIGITS - 1);

    /* Put the torches high! */
    max7219_intensity_max();

    /* Crank up the chip */
    max7219_crank();

    /* Clear all digits */
    max7219_clear_all();

    /* Let the chip boot in peace */
    max7219_noop();
}


inline
void  max7219_clear_all(void)
{
    for ( int  digit = 1; digit <= MAX7219_DIGITS; digit++ )
        max7219_clear(digit);
}


void  max7219_push_byte(uint8_t  byte)
{
    int     digit;


    for ( digit = 7; digit >= 0; digit-- ) {
        MAX7219_PORT_CLK &= ~(1 << MAX7219_PIN_CLK);
        if ( byte & (1 << digit) )
            MAX7219_PORT_DATA |= (1 << MAX7219_PIN_DATA);
        else
            MAX7219_PORT_DATA &= ~(1 << MAX7219_PIN_DATA);
        MAX7219_PORT_CLK |= (1 << MAX7219_PIN_CLK);
#ifdef MAX7219_SLOW_CLOCK
        _delay_ms(10);
#else
        asm volatile ("nop");
#endif
    };
}


inline
void  max7219_push(uint8_t  regaddr, uint8_t  data)
{
    MAX7219_PORT_LOAD &= ~(1 << MAX7219_PIN_LOAD);
    max7219_push_byte(regaddr);
    max7219_push_byte(data);
    MAX7219_PORT_LOAD |= (1 << MAX7219_PIN_LOAD);
}


void  max7219_digits(uint8_t  *digits)
{
    uint8_t     digit;


    digit = 0;
    while ( digit < MAX7219_DIGITS )
        max7219_digit(digit + 1, digits[digit]);
}


void  max7219_helo(void)
{
    int     digit = 0;


    if ( MAX7219_DIGITS > 4 )
        for ( digit = 1; digit <= (MAX7219_DIGITS - 4) / 2; digit++ )
            max7219_digit(digit, MAX7219_CHAR_BLANK);

    max7219_digit(++digit, MAX7219_CHAR_H);
    max7219_digit(++digit, MAX7219_CHAR_E);
    max7219_digit(++digit, MAX7219_CHAR_L);
    max7219_digit(++digit, 0);
}

/* vim: set filetype=c expandtab tabstop=4 sts=4 shiftwidth=4 noai nocp: */
