max7219
=======

MAX7219 7-segment display BCD driver for ATmega microcontrollers.


### Description

This is a simple, MAX7219 driver for ATmega microcontrollers. Currently only
BCD mode and 7-segment display is supported.

### Usage

Before use, library must be configured by uncommenting and editing settings
in max7219_config.h file. You can find descriptions of each setting there.
Included example.c file contains few simple examples and can be used for
testing. Keep in mind, however, that it requires four 7-segment displays to
work properly.
