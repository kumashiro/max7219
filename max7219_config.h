#ifndef MAX7219_CONFIG_H
#define MAX7219_CONFIG_H

/* Set boot delay (in miliseconds) */
//#define MAX7219_BOOT_TIME       0

/* Define this to use much slower, time based clock */
//#define MAX7219_SLOW_CLOCK

/* Set the number of digits */
//#define MAX7219_DIGITS          4

/***
 * Pin configurations
 *
 * MAX7219 is using three pins for communication:
 *   DATA (also known as DIN) is an internal shift
 *   register input. We'll feed data through it.
 *
 *   CLK (or CLOCK) is an input pin for clock.
 *
 *   LOAD (or CS) is used to block the internal
 *   shift register output, so shifted bits are
 *   not propagated during transfer.
 *
 * Pin numbers for DIP24:
 *   DATA       1
 *   CLK        13
 *   LOAD       12
 ***/

/* Set port with DATA pin */
//#define MAX7219_PORT_DATA       PORTB

/* Set direction register for DATA */
//#define MAX7219_DREG_DATA       DDRB

/* Set pin for DATA */
//#define MAX7219_PIN_DATA        PB2

/* Set port with CLK pin */
//#define MAX7219_PORT_CLK        PORTB

/* Set direction register for CLK */
//#define MAX7219_DREG_CLK        DDRB

/* Set pin for CLK */
//#define MAX7219_PIN_CLK         PB1

/* Set port with LOAD pin */
//#define MAX7219_PORT_LOAD       PORTB

/* Set direction register for LOAD */
//#define MAX7219_DREG_LOAD       DDRB

/* Set pin for LOAD */
//#define MAX7219_PIN_LOAD        PB0

#endif  /* MAX7219_CONFIG_H */

/* vim: set filetype=cpp expandtab tabstop=4 sts=4 shiftwidth=4 noai nocp */
