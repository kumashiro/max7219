/*
 * Copyright (C) 2014 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License Version
 * 3.0 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <avr/io.h>
#include <util/delay.h>

#include "max7219_config.h"
#include "max7219.h"


/***
  Display int16_t value on 4 digit LED display
 ***/
static
void  display_value(int16_t  value)
{
    uint8_t     digit;


    for ( digit = MAX7219_DIGITS; digit > 0; digit-- ) {
        if ( value < 0 )
            break;
        max7219_digit(digit, value % 10);
        value /= 10;
    };
}


int  main(void)
{
    uint8_t     i;


    /* Initialize MAX7219 and setup pins */
    max7219_init();
    
    /* Build-in test feature of MAX7219 */
    max7219_test_on();
    _delay_ms(1000);
    max7219_test_off();

    /* Display HELO message */
    max7219_helo();
    _delay_ms(2000);
    max7219_clear_all();

    /* Count from 0 to 9 on a last display */
    for ( i = 0; i < 10; i++ ) {
        max7219_digit(MAX7219_DIGITS, i);
        _delay_ms(500);
    }
    max7219_clear_all();

    /* Display value with a decimal dot */
    for ( i = 1; i <= MAX7219_DIGITS; i++ )
        if ( i == 2 )
            max7219_digit(i, i | MAX7219_DECIMAL);
        else
            max7219_digit(i, i);

    for ( i = 15; i > 0; i-- ) {
        max7219_intensity(i);
        _delay_ms(50);
    };

    /* Fade out and back */
    for ( i = 0; i < 16; i++ ) {
        max7219_intensity(i);
        _delay_ms(50);
    };
    max7219_clear_all();

    /* Count from 0 to 9999 at full speed */
    for ( uint16_t  j = 0; j < 10000; j++ )
        display_value(j);
    _delay_ms(1000);
    max7219_clear_all();

    /* Shut the chip down for 5 seconds */
    max7219_shutdown();
    _delay_ms(5000);
    max7219_crank();

    /* Display four "-" slowly */
    for ( i = 1; i <= MAX7219_DIGITS; i++ ) {
        max7219_digit(i, MAX7219_CHAR_MINUS);
        _delay_ms(100);
    };

    /* Flash (to infinity) */
    i = 1;
    while ( 1 ) {
        i = i ? 0 : 15;
        max7219_intensity(i);
        _delay_ms(100);
    };

    return 0;
}

/* vim: set filetype=c expandtab tabstop=4 sts=4 shiftwidth=4 noai nocp: */
